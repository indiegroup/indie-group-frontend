import Vue from "vue";
import Vuex from "vuex";
//import non-standard packages
import VueCookies from "vue-cookies";
import createPersistedState from "vuex-persistedstate";
import VueLodash from "vue-lodash";
import lodash from "lodash";
//import modules
import authentication from "./modules/authentication";
import datapoint from "./modules/datapoint";
import magentoData from "./modules/magentoData";
import client from "./modules/client";
import store from "./modules/store";
import teamleader from "./modules/teamleader";

Vue.use(Vuex);
Vue.use(VueCookies);
Vue.use(VueLodash, { lodash: lodash });

VueCookies.config("2d");

export default new Vuex.Store({
  state: {
    baseUrl: "http://dashboard-dev.indiegroup.be/",
  },
  modules: {
    // module registration
    authentication,
    datapoint,
    magentoData,
    client,
    store,
    teamleader,
  },
  // persistedState zorgt ervoor dat bij een refresh de state behouden blijft, dit is ook het geval voor LT (meer dan 1 dag)
  plugins: [createPersistedState()],
});
