//import router
import router from "../../router";

export default {
  state: {
    isAuthenticated: false,
    isAdministrator: false,
    bearerToken: "",
    userName: "",
  },

  getters: {
    isAuthenticated: (state) => state.isAuthenticated,
    isAdministrator: (state) => state.isAdministrator,
    userName: (state) => state.userName,
  },

  mutations: {
    handleLogin: function(state, payload) {
      console.log("login met data: ", payload);
      if (payload.error_code == 400) {
        //niets doen want login is niet gelukt.
      } else {
        $cookies.set("BearerToken", payload.jwt, 604800, null, null, null);
        state.bearerToken = payload.jwt;
        state.isAuthenticated = true;
        state.userName = payload.name;
        if (payload.role_id == 1) {
          state.isAdministrator = true;
        }
      }
    },

    handleLogout: function(state) {
      router.push({ path: "login" });
      $cookies.remove("BearerToken");
      state.bearerToken = null;
      state.isAuthenticated = false;
      console.log("user logged out");
    },
  },

  actions: {
    async login({ commit }, formdata) {
      const response = await fetch(`https://modapi.io/api/login`, {
        headers: new Headers({
          Accept: `application/json`,
        }),
        method: "post",
        body: formdata,
      }).then((r) => r.json());
      this.commit("handleLogin", response);
      return response;
    },

    logout({ commit }) {
      this.commit("handleLogout");
    },

    async register({ commit }, formdata) {
      const response = await fetch(`https://modapi.io/api/register`, {
        headers: new Headers({
          Accept: `application/json`,
        }),
        method: "post",
        body: formdata,
      }).then((r) => r.json());
      console.log("register tryout: ", response);
      //this.commit("handleLogin", response);
      return response;
    },

    async checkAuthentication({ commit }) {
      if ($cookies.get("BearerToken") != null) {
        const validation = await this.dispatch("validateToken");
        validation.jwt = $cookies.get("BearerToken");
        if (validation.error_code == 401) {
          this.commit("handleLogout");
        } else {
          this.commit("handleLogin", validation);
        }
      }
    },

    async validateToken({ commit }) {
      const response = await fetch(`https://modapi.io/api/jwtLogin`, {
        headers: new Headers({
          Accept: `application/json`,
          Authorization: $cookies.get("BearerToken"),
        }),
        method: "get",
      }).then((r) => r.json());
      return response;
    },
  },
};
