export default {
  state: {
    magentoData: [],
  },

  getters: {
    magentoData: (state) => state.magentoData,
  },

  mutations: {
    setMagentoData(state, payload) {
      state.magentoData = payload;
    },
  },

  actions: {
    async getMagentoData({ commit }) {
      const response = await fetch(
        `https://modapi.io/api/orders?store_id=1&date_from=2020-04-20&date_to=2020-04-28`,
        {
          headers: new Headers({
            Accept: `application/json`,
            Authorization: $cookies.get("BearerToken"),
          }),
          method: "GET",
        }
      ).then((r) => r.json());

      this.commit("setMagentoData", response);
      console.log("order response: ", response);
    },
  },
};
