export default {
  state: {
    teamleaderClients: {},
  },
  getters: {
    teamleaderClients: (state) => state.teamleaderClients,
  },
  mutations: {
    setTeamleaderClients(state, payload) {
      state.teamleaderClients = payload;
    },
  },
  actions: {
    async getTeamderClientsByName({ commit }, name) {
      console.log("name die gezocht wordt", name);
      const response = await fetch(
        `https://modapi.io/api/clientTeamleader?name=` + name,
        {
          headers: new Headers({
            Accept: `application/json`,
            Authorization: $cookies.get("BearerToken"),
          }),
          method: "GET",
        }
      ).then((r) => r.json());

      this.commit("setTeamleaderClients", response);
      console.log("teamleaderClients response: ", response);
      return response;
    },
  },
};
