export default {
  state: {
    applications: [],
    dataPoints: [],
  },

  getters: {
    applications: (state) => state.applications,
    dataPoints: (state) => state.dataPoints,
  },

  mutations: {
    setApplications(state, payload) {
      state.applications = payload;
    },

    setDataPoints(state, payload) {
      state.dataPoints = payload;
    },
  },

  actions: {
    //applications
    async getApplications({ commit }) {
      const response = await fetch(`https://modapi.io/api/applications`, {
        headers: new Headers({
          Accept: `application/json`,
        }),
        method: "GET",
      }).then((r) => r.json());

      this.commit("setApplications", response);
      console.log("response: ", response);
    },

    //data points
    async getDataPoints({ commit }) {
      const response = await fetch(
        `https://modapi.io/api/dataPoint?user_id=current_user`,
        {
          headers: new Headers({
            Accept: `application/json`,
            Authorization: $cookies.get("BearerToken"),
          }),
          method: "GET",
        }
      ).then((r) => r.json());

      console.log("response: ", response);
      this.commit("setDataPoints", response);
    },

    async getDataPointById({ commit }, dataPointId) {
      const response = await fetch(
        `https://modapi.io/api/dataPoint?id=` + dataPointId,
        {
          headers: new Headers({
            Accept: `application/json`,
            Authorization: $cookies.get("BearerToken"),
          }),
          method: "GET",
        }
      ).then((r) => r.json());

      console.log("response: ", response);
    },

    async createDataPoint({ commit }, payload) {},

    async updateDataPoint({ commit }, payload) {
      const response = await fetch(`https://modapi.io/api/dataPoint`, {
        headers: new Headers({
          Accept: `application/json`,
          Authorization: $cookies.get("BearerToken"),
        }),
        method: "PUT",
        body: payload,
      }).then((r) => r.json());

      console.log("response: ", response);
    },
  },
};
