export default {
  state: { clients: {} },

  getters: {
    clients: (state) => state.clients,
  },

  mutations: {
    setClients(state, payload) {
      state.clients = payload;
    },
  },

  actions: {
    //client information
    async getClients({ commit }) {
      const response = await fetch(`https://modapi.io/api/clients?limit=100`, {
        headers: new Headers({
          Accept: `application/json`,
          Authorization: $cookies.get("BearerToken"),
        }),
        method: "GET",
      }).then((r) => r.json());

      this.commit("setClients", response);
      return response;
    },

    async getClientsByName({ commit }, name) {
      const response = await fetch(
        `https://modapi.io/api/clients?name=` + name + `&limit=100`,
        {
          headers: new Headers({
            Accept: `application/json`,
            Authorization: $cookies.get("BearerToken"),
          }),
          method: "GET",
        }
      ).then((r) => r.json());

      this.commit("setClients", response);
      return response;
    },

    async updateClient({ commit }, payload) {
      const response = await fetch(`https://modapi.io/api/clients`, {
        headers: new Headers({
          Accept: `application/json`,
          Authorization: $cookies.get("BearerToken"),
        }),
        method: "PUT",
        body: payload,
      }).then((r) => r.json());
      this.dispatch("getAllClientOverview");
    },

    async addClient({ commit }, payload) {
      const response = await fetch(`https://modapi.io/api/clients`, {
        headers: new Headers({
          Accept: `application/json`,
          Authorization: $cookies.get("BearerToken"),
        }),
        method: "POST",
        body: payload,
      }).then((r) => r.json());
      return response;
    },
  },
};
