//import router
import router from "../../router";

export default {
  state: { storeOverview: [] },

  getters: {
    storeOverview: (state) => state.storeOverview,
  },

  mutations: {
    setStoreOverview(state, payload) {
      state.storeOverview = payload;
    },
  },

  actions: {
    async getAllStoreOverview({ commit }) {
      const response = await fetch(
        `https://modapi.io/api/storeOverview?limit=15`,
        {
          headers: new Headers({
            Accept: `application/json`,
            Authorization: $cookies.get("BearerToken"),
          }),
          method: "GET",
        }
      ).then((r) => r.json());

      this.commit("setStoreOverview", response);
      console.log("storeOverview response: ", response);
      return response;
    },

    async addStore({ commit }, payload) {
      console.log("addstore called!");
      const response = await fetch(`https://modapi.io/api/stores`, {
        headers: new Headers({
          Accept: `application/json`,
          Authorization: $cookies.get("BearerToken"),
        }),
        method: "POST",
        body: payload,
      }).then((r) => r.json());
      console.log("store toegevoegd: ", response);
      if (response.store_id) {
        router.push({ path: "/" });
      }
      return response;
      // this.dispatch("getAllClientOverview");
    },
  },
};
