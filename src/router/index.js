import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store/index";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    meta: { requiresAuth: true },
    component: () => import(/* webpackChunkName: "home" */ "../views/Home.vue"),
  },
  {
    path: "/addDataPoint",
    name: "AddDataPoint",
    meta: { requiresAuth: true },
    component: () =>
      import(
        /* webpackChunkName: "adddatapoint" */ "../views/AddDataPoint.vue"
      ),
  },
  {
    path: "/addDatastream",
    name: "AddDatastream",
    meta: { requiresAuth: true },
    component: () =>
      import(
        /* webpackChunkName: "adddatastream" */ "../views/AddDatastream.vue"
      ),
  },
  {
    path: "/updateDataPoint/:dataPointId",
    name: "UpdateDatapoint",
    meta: { requiresAuth: true },
    component: () =>
      import(
        /* webpackChunkName: "updatedatapoint" */ "../views/UpdateDataPoint.vue"
      ),
  },
  {
    path: "/login",
    name: "Login",
    component: () =>
      import(/* webpackChunkName: "login" */ "../views/Login.vue"),
  },
  {
    path: "/register",
    name: "Register",
    meta: { requiresAuth: false },
    component: () =>
      import(/* webpackChunkName: "register" */ "../views/Register.vue"),
  },
  {
    path: "/soapUser",
    name: "SoapUser",
    component: () =>
      import(/* webpackChunkName: "soapuser" */ "../views/SoapUser.vue"),
  },

  {
    path: "/chartjs",
    name: "chartjs",
    component: () =>
      import(/* webpackChunkName: "chartjs" */ "../views/Chartjs.vue"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    if (!store.getters.isAuthenticated)
      next({
        path: "/login",
        query: { redirect: to.fullPath },
      });
    else next();
  } else {
    next();
  }
});

export default router;
